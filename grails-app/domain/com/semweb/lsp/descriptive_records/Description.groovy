package com.semweb.lsp.descriptive_records

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria

import com.semweb.lsp.semanticweb.Authority

@Slf4j
class Description implements MultiTenant<Description> {

  String id
  String title
  Authority owner
  byte[] data

  static mapping = {
     table "lsp_description"
            id column: 'des_id', generator: 'uuid2', length:36
         title column: 'des_title'
         owner column: 'des_owner_authority_fk'
          data column: 'des_data'
  }

  static constraints = {
    title nullable:false, blank:false
    owner nullable:false, blank:false
    data nullable:true, blank:false
  }

  static query_config = [
    defaultField:'title',
    properties:[
      title:[mode:'keyword']
    ]
  ]

  static graphql = [
    queries:[
      'findDescriptionByLuceneQuery':[ methodName:'findAllByLuceneQueryString',
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findDescriptionByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

}
