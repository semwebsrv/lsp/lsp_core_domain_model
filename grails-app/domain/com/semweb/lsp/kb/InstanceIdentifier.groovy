package com.semweb.lsp.kb

import grails.gorm.MultiTenant
import groovy.util.logging.Slf4j
import com.semweb.lsp.semanticweb.Authority

@Slf4j
class InstanceIdentifier implements MultiTenant<InstanceIdentifier> {

  String id
  Instance instance
  Authority authority
  String value

  static mapping = {
     table "lsp_instance_identifier"
            id column: 'ii_id', generator: 'uuid2', length:36
      instance column: 'ii_instance_fk'
     authority column: 'ii_authority_fk'
         value column: 'ii_value'
  }

  static constraints = {
    instance nullable:false
    authority nullable:false
    value nullable:false, blank:false
  }
  

  public static String normalise(Authority a, String value) {
    return value;
  }

  public static InstanceIdentifier lookup(String authority, String value) {

    InstanceIdentifier result = null;
    Authority a = Authority.lookup(authority)

    if ( a != null ) {
      def value_to_lookup = normalise(a, value)
      result = InstanceIdentifier.findByAuthorityAndValue(a, value_to_lookup)
    }

    return result;
  }
}
