package com.semweb.lsp.kb

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria

import com.semweb.lsp.descriptive_records.Description

@Slf4j
class InstanceDescription implements MultiTenant<InstanceDescription> {

  String id
  Instance instance
  Description description

  static mapping = {
     table "lsp_instance_description"
            id column: 'id_id', generator: 'uuid2', length:36
         instance column: 'id_instance'
      description column: 'id_description'
  }

  static constraints = {
       instance nullable:false, blank:false
    description nullable:false, blank:false
  }
  
}
