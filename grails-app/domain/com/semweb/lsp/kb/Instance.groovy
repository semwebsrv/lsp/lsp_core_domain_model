package com.semweb.lsp.kb

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria
import holo.shared.refdata.RefdataValue

@Slf4j
class Instance implements MultiTenant<Instance> {

  String id
  String title
  Work work

  // Print or Electronic
  RefdataValue medium

  String edition;

  // Publication Model - Traditional|OpenAccess
  RefdataValue publicationModel

  Instance replacedBy

  String authority
  String confidence

  static mapping = {
     table "lsp_instance"
                   id column: 'ins_id', generator: 'uuid2', length:36
                title column: 'ins_title'
                 work column: 'ins_work_fk'
               medium column: 'ins_medium_fk'
              edition column: 'ins_edition'
     publicationModel column: 'ins_pub_model_fk'
           replacedBy column: 'ins_replaced_by_fk'
            authority column: 'ins_authority'
           confidence column: 'ins_confidence'
  }

  static constraints = {
               title nullable:false, blank:false
                work nullable:true, blank:false
              medium nullable: true
             edition nullable: true
    publicationModel nullable: true
          replacedBy nullable: true
           authority nullable: true
          confidence nullable: true
  }
  
  static hasMany = [
    descriptions: InstanceDescription
  ]

  static mappedBy = [
    descriptions: 'instance'
  ]

  static query_config = [
    defaultField:'title',
    properties:[
      title:[mode:'keyword']
    ]
  ]

  static graphql = [
    queries:[
      'findInstanceByLuceneQuery':[ methodName:'findAllByLuceneQueryString',
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findInstanceByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

}
