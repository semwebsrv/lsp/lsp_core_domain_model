package com.semweb.lsp.kb

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria

import holo.shared.refdata.RefdataValue
import holograilsutils.TextUtils


@Slf4j
class Work implements MultiTenant<Work> {

  String id
  String title
  String normalisedTitle

  // Books and Monographs, Journals, etc
  RefdataValue media  


  static mapping = {
     table "lsp_work"
                 id column: 'wrk_id', generator: 'uuid2', length:36
              title column: 'wrk_title'
              media column: 'wrk_media_rdv_fk'
    normalisedTitle column: 'wrk_norm_title'
  }

  static constraints = {
    title nullable:false, blank:false
    media nullable:true

    // In practice, normalised title will always be filled out by the beforeInsert/beforeUpdate trigger
    normalisedTitle nullable:true
  }

  static query_config = [
    defaultField:'title',
    properties:[
      title:[mode:'keyword']
    ]
  ]

  static graphql = [
    queries:[
      'findWorkByLuceneQuery':[ methodName:'findAllByLuceneQueryString',
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findWorkByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

   def beforeInsert(){
     println("beforeInsert - normalise ${this.title}");
     this.normalisedTitle = TextUtils.normaliseString(this.title)
   }

   def beforeUpdate(){
     println("beforeUpdate - normalise ${this.title}");
     this.normalisedTitle = TextUtils.normaliseString(this.title)
   }
}
