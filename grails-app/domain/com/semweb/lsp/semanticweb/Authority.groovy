package com.semweb.lsp.semanticweb

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria

@Slf4j
class Authority implements MultiTenant<Authority> {

  String id
  String name
  Boolean lowercase
  Boolean nopunc
  Boolean trim

  // implied high level resource type
  String impliedType

  // Values using this authority imply a specific Media type - BKM, Journal, etc - eg isbn = book
  String impliedMedia 

  // Values using this authority imply a specific Medium - eg eIssn = electronic
  String impliedMedium

  // If you encounter this type, USE this type instead - used to point non-preferred values to our preferred alternative
  String useInstead


  // The defaults for authorities we know about
  private static Map known_auth_data = [
    'isni':     [name:'isni',  lowercase:Boolean.TRUE, nopunc:Boolean.TRUE, trim:Boolean.TRUE ],
    'eissn':    [name:'issn',  lowercase:Boolean.TRUE, nopunc:Boolean.TRUE, trim:Boolean.TRUE, impliedType:'bib', impliedMedia:'serial', impliedMedium:'electronic', useInstead:'issn' ],
    'isbn':     [name:'isbn',  lowercase:Boolean.TRUE, nopunc:Boolean.TRUE, trim:Boolean.TRUE, impliedType:'bib', impliedMedia:'book'],
    'issn':     [name:'issn',  lowercase:Boolean.TRUE, nopunc:Boolean.TRUE, trim:Boolean.TRUE, impliedType:'bib', impliedMedia:'serial', impliedMedium:'print'],
    'doi':      [name:'doi',   impliedType:'bib', impliedMedium:'electronic']
  ]

  // byte[] data

  static mapping = {
     table "lsp_authority"
               id column: 'auth_id', generator: 'uuid2', length:36
             name column: 'auth_name'
        lowercase column: 'auth_lowercase'
           nopunc column: 'auth_nopunc'
             trim column: 'auth_trim'
      impliedType column: 'auth_implied_type'
     impliedMedia column: 'auth_implied_media'
    impliedMedium column: 'auth_implied_medium'
       useInstead column: 'auth_use'
  }

  static constraints = {
             name nullable:false, blank:false
        lowercase nullable:true
           nopunc nullable:true
             trim nullable:true
      impliedType nullable:true
     impliedMedia nullable:true
    impliedMedium nullable:true
       useInstead nullable:true
  }

  static query_config = [
    defaultField:'name',
    properties:[
      title:[mode:'name']
    ]
  ]

  static graphql = [
    queries:[
      'findAuthorityByLuceneQuery':[ methodName:'findAllByLuceneQueryString',
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findAuthorityByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

  public static Authority lookupOrCreate(String name) {
    Authority result = Authority.findByName(name?.toLowerCase());
    if ( result == null ) {
      // If we have any authority specific defaults, apply them here
      if ( known_auth_data[name] != null ) {
        result = new Authority(known_auth_data[name]).save(flush:true, failOnError:true)
      }
      else {
        result = new Authority(name: name.toLowerCase()).save(flush:true, failOnError:true);
      }
    }

    return result;
  }

}
