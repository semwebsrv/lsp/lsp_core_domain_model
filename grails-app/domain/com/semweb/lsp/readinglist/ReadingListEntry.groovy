package com.semweb.lsp.readinglist

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria
import holo.shared.refdata.RefdataValue

@Slf4j
class ReadingListEntry implements MultiTenant<ReadingListEntry> {

  String id
  String title

  Date dateFirstSeen
  Date dateRemoved

  String moduleCode     
  String orderLineNote
  String numStudents
  String fundCode
  String locationCode
  String libraryCode
  String libraryName
  String exceptions
  Boolean prb // possiblyReviewedBefore
  String priority
  Integer suggestedQty
  Integer numHeld
  String isbn
  String isbn13
  String lcn
  String duplicateMatch

  // These will move to "eBookOffer"
  // Duplication Match (added by GOBI)
  // eAvailability (added by GOBI)
  // DDA Eligibility (added by GOBI)
  // Newer Edition Available (added by GOBI)
  // Quantity p
  // Quantity e
  // Price p
  // Price e

  String noteForLibrary
  String noteForStudernmt
  String edition
  String pubDate

  String itemLink

  // Total Number of Items
  // No. Items in Site0
  // No. Items in Site1
  // No. Items in Site2
  // No. Items in Site2
  // No. Items in Site3
  // Item Link


  // Unprocessed
  ReadingList owner
  RefdataValue entryStatus

  static mapping = {
     table "lsp_reading_list_entry"

               id column: 'rle_id', generator: 'uuid2', length:36
            title column: 'rle_title'
    dateFirstSeen column: 'rle_date_first_seen'
      dateRemoved column: 'rle_removed'
       moduleCode column: 'rle_module_code'
    orderLineNote column: 'rle_order_line_note'
      numStudents column: 'rle_number_students'
         fundCode column: 'rle_fund_code'
     locationCode column: 'rle_location_code'
      libraryCode column: 'rle_library_code'
      libraryName column: 'rle_library_name'
       exceptions column: 'rle_exceptions'
              prb column: 'rle_prb'
         priority column: 'rle_priority'
     suggestedQty column: 'rle_suggested_qty'
          numHeld column: 'rle_num_held'
             isbn column: 'rle_isbn'
           isbn13 column: 'rle_isbn_13'
              lcn column: 'rle_lcn'
   duplicateMatch column: 'rle_duplicate_match'
   noteForLibrary column: 'rle_note_for_library'
   noteForStudent column: 'rle_note_for_student'
          edition column: 'rle_edition'
          pubDate column: 'rle_pub_date'
         itemLink column: 'rle_item_link'


            owner column: 'rle_owner_fk'
      entryStatus column: 'rle_status_fk'
  }

  static constraints = {
            title nullable:false, blank:false
    dateFirstSeen nullable:true
      dateRemoved nullable:true
       moduleCode nullable:true
    orderLineNote nullable:true
      numStudents nullable:true
         fundCode nullable:true
     locationCode nullable:true
      libraryCode nullable:true
      libraryName nullable:true
       exceptions nullable:true
              prb nullable:true
         priority nullable:true
     suggestedQty nullable:true
          numHeld nullable:true
             isbn nullable:true
           isbn13 nullable:true
              lcn nullable:true
   duplicateMatch nullable:true
   noteForLibrary nullable:true
   noteForStudent nullable:true
          edition nullable:true
          pubDate nullable:true
         itemLink nullable:true
           owner nullable:false
     entryStatus nullable:false
  }

  static query_config = [
    defaultField:'title',
    properties:[
      title:[mode:'keyword']
    ]
  ]

  static graphql = [
    queries:[
      'findReadingListEntryByLuceneQuery':[ methodName:'findAllByLuceneQueryString',
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findReadingListEntryByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

}
