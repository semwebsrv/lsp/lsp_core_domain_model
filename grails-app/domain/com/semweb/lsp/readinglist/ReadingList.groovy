package com.semweb.lsp.readinglist

import grails.gorm.MultiTenant
import grails.gorm.annotation.Entity
import holo.core.traits.ACLControlled
import holo.core.parties.Party
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

import org.hibernate.criterion.DetachedCriteria
import org.grails.datastore.mapping.model.PersistentEntity
import org.hibernate.criterion.Restrictions
import org.hibernate.criterion.Subqueries
import org.hibernate.criterion.Projections
import org.hibernate.Criteria

@Slf4j
class ReadingList implements MultiTenant<ReadingList> {

  String id
  String title
  String code
  Date lastUpdated

  static mapping = {
     table "lsp_reading_list"

             id column: 'rl_id', generator: 'uuid2', length:36
          title column: 'rl_title'
           code column: 'rl_code'
    lastUpdated column: 'rl_last_updated'
  }

  static constraints = {
          title nullable:false, blank:false
           code nullable:true, blank:false
    lastUpdated nullable:true, blank:false
  }

  static hasMany = [
    entries: ReadingListEntry
  ]

  static mappedBy = [
    entries: 'owner'
  ]

  static query_config = [
    defaultField:'title',
    properties:[
      title:[mode:'keyword']
    ]
  ]

  static graphql = [
    queries:[
      'findReadingListByLuceneQuery':[ methodName:'findAllByLuceneQueryString',
                                     args:[ [ type:String.class, param_name:'luceneQueryString' ] ],
                                     addContext:true ],
      'findReadingListByTitle':[ methodName:'findAllByTitle', args:[ [ type:String.class, param_name:'title' ] ] ]
    ]
  ]

}
