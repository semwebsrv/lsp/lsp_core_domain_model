package com.semweb.lsp.kb;

import com.semweb.lsp.kb.Work;
import com.semweb.lsp.kb.Instance;
import com.semweb.lsp.semanticweb.Authority;
import holo.shared.refdata.RefdataValue;

public class ResolverService {

  /**
   * Given a list of bibliographic properties make an absolute best guess which instance
   * the collection of descriptive properties best matches. If confidence is so low that a guess
   * cannot be arrived at, then throw an exception. Callers should be aware that this function
   * is in no way idempotent - what works one day may not work the next - a single title string
   * may be sufficient one day, but not be so the next.
   *
   * Iterate through identifiers and resolve each to an instance, each match against an
   *
   * descriptive_properties : {
   *   title: 'x',
   *   identifiers: [
   *     { authority: 'a', value:'v' },
   *     { authority: 'a', value:'v' },
   *   ]
   * }
   * 
   * 
   * Returns
   * 
   * [
   *   { instanceId: x, matches: y }
   * ]
   */
  public Map resolveInstances(Map descriptive_properties) {
    List result = [
    ]

    descriptive_properties?.identifiers?.each { entry ->

      Instance candidate_instance = resolveInstanceByIdentifier(entry.authority, entry.value)

      if ( candidate_instance ) {
        def candidate_record = result.find { it.instanceId == candidate.id }
        if ( candidate_record != null ) {
          candidate_record.matches++
        }
        else {
          result.add ( [ instanceId: candidate_instance.id, matches:1 ] )
        }
      }
    }

    return result;
  }


  /**
   * descriptive_properties::map {
   *   title: String,
   *   variantTitles: [{lang:String, value: String}...],
   *   mediaType: String,
   *   identifiers: [
   *     { authority: 'a', value:'v' },
   *     { authority: 'a', value:'v' },
   *   ]
   * }
   */
  public List resolveWorks(Map descriptive_properties) {

    List result = []

    descriptive_properties?.identifiers?.each { entry ->

      Instance candidate_instance = resolveInstanceByIdentifier(entry.authority, entry.value)
      Work work = candidate_instance.work

      if ( work ) {
        def candidate_record = result.find { it.workId == work.id }
        if ( candidate_record != null ) {
          candidate_record.matches++
        }
        else {
          result.add ( [ workId: work.id, matches:1 ] )
        }
      }
    }

    return result;
  }

  /**
   * Lookup the instance attached to an identifier
   */
  public Instance resolveInstanceByIdentifier(String authority, String value) {
    InstanceIdentifier ii = null;
    List<InstanceIdentifier> ii_list = InstanceIdentifier.executeQuery('select ii from InstanceIdentifier as ii where ii.authority.name=:a and ii.value=:v',[a:authority,v:value])

    if ( ii_list.size() == 1 )
      ii = ii_list.get(0);

    // if ( ii == null) {
    //   ii = addIdentifier(authority,value);
    // }

    return ii?.instance
  }

  /**
   * Find out as much as possible about the given identifier and create/link to any instance records
   */
  private InstanceIdentifier addIdentifier(String authority, String value) {
    InstanceIdentifier result = null;

    def instance_metadata = lookupInstanceMetadtaViaGlobalSources(authority,value);

    return result;
  }

  private Map lookupInstanceMetadtaViaGlobalSources(String authority, String value) {
    Map result = null;
    return result;
  }
  

  /**
   * https://openlibrary.org/
   * Useful for trying to look up information on books and monographs - useful when authority=isbn,OCLC,LCCN,OLID
   * https://openlibrary.org/api/books?bibkeys=ISBN:0201558025&format=json&jscmd=data
   */
  private lookupInstanceOnOpenLibrary(String authority, String value) {
    // See https://openlibrary.org/developers/api
  }


  /**
   * bestEffortsMatch - Try to make sense of a load of bib properties in order to identify the works and instances 
   * being referred to.
   *
   * @returns Map result = [
   *   possibleWorks:[
   *     [ workId: UUID, work: OBJECT, instances: [ OBJECT,... ], matches:int],...
   *   ],
   *   possibleInstances:[
   *     [ instanceId: UUID, matches: int, instance: OBJECT, matched_identifiers: [ [ authority:STRING, value:STRING ],... ] ]
   *   ],
   *   unmatchedIdentifiers:[
   *     [ authority:STRING, value:STRING ],...
   *   ]
   * ]
   *
   */
  public Map bestEffortsMatch(Map descriptive_properties) {
    Map result = [
      possibleWorks:[],
      possibleInstances:[],
      unmatchedIdentifiers:[]
    ]

    descriptive_properties?.identifiers?.each { entry ->

      Instance candidate_instance = resolveInstanceByIdentifier(entry.authority, entry.value)

      if ( candidate_instance ) {

        def candidate_instance_info = result.possibleInstances.find { it.instanceId == candidate_instance.id }
        if ( candidate_instance_info != null ) {
          candidate_instance_info.matches++;
          candidate_instance_info.matched_identifiers.add([ authority:entry.authority, value:entry.value ]);
        }
        else {
          result.possibleInstances.add( [ instanceId: candidate_instance.id, matches: 1, instance: candidate_instance, matched_identifiers:[ [ authority:entry.authority, value:entry.value ] ] ] )
        }

        Work work = candidate_instance.work

        if ( work ) {
          def candidate_work = result.possibleWorks.find { it.workId == work.id }
          if ( candidate_work != null ) {
            candidate_work.matches++
            candidate_work.instances.add(candidate_instance)
          }
          else {
            result.possibleWorks.add( [ 'matches':1, 'workId': work.id, 'work': work, 'instances': [ candidate_instance ]  ] )
          }
        }
        else {
          result.unmatchedIdentifiers.add(entry);
          
        }
      }
      else {
        result.unmatchedIdentifiers.add(entry);
      }
    }
    
    return result;
  }

  Work workByDescriptiveProperties(Map props) {
    Work result = null;
    if ( ( props.title != null ) && ( props.media ) ) {
      def qr = Work.executeQuery('select w from Work as w where w.title=:t and w.media.value=:m',[t:props.title, m:props.media])
      if ( qr.size() == 1 ) {
        result = qr.get(0);
      }
    }

    if ( result == null && props.title != null )
      result = Work.findByTitle(props.title) 

    if ( result == null ) {
      def media_rdv = null;
      if ( props.media != null ) {
        media_rdv = RefdataValue.lookupOrCreate('mediatype', props.media, props.media)
      }
      result = new Work(title: props.title, media: media_rdv).save(flush:true, failOnError:true);
    }

    return result;
  }

  private String inferMediaFromIdentfiiers(List identifiers) {
    String result = null;
    identifiers.each { id ->
      Authority a = Authority.lookupOrCreate(id.authority)
      if ( a.impliedMedia != null ) {
        if ( result != null ) {
          if ( result != a.impliedMedia )
            throw new RuntimeException("Citation contains identifiers which imply different undelying media types (EG Book and Journal). Unable to contine");
        }
        else {
          result = a.impliedMedia;
        }
      }
      
    }
    return result;
  }

}
