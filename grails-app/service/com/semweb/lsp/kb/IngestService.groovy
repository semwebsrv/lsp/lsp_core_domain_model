package com.semweb.lsp.kb;

import com.semweb.lsp.kb.Work;
import com.semweb.lsp.kb.Instance;
import holo.shared.refdata.RefdataValue;
import com.semweb.lsp.semanticweb.Authority;
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import static groovyx.net.http.HttpBuilder.configure

public class IngestService {

  ResolverService resolverService

  /**
   * A caller has a bibliographic record that should describe an instance with a title and some identifiers.
   * Sometimes, we are passed descriptions that contain identifiers for the same work, but for different instances,
   * commonly, the same journal or book, but with identifiers for the print and electronic edition mixed up.
   * The job of this method is to unpick that mess, and to make sure that we create the right instances. 
   */
  public void absorb(Map descriptive_properties) {

    // At the start, we have not identified a work
    Work work = null;

    // Look up as much as we possibly can about the item
    Map existing_info = resolverService.bestEffortsMatch(descriptive_properties);

    // PASS 1 : Ensure that we know as much about this item as our registries do - cross reference in all information possible

    switch ( existing_info.possibleWorks.size() ) {
      case 0:
        // Take no action yet - hopfully one of the external registries will provide us with an authoritative record
        // for the work.
        work = resolverService.workByDescriptiveProperties(descriptive_properties);
        if ( work == null ) {
          // We should attempt to infer a medium from the identifiers - ISBN=BOOK/Monograph, ISSN=Serial
          work = new Work(title: descriptive_properties.title).save(flush:true, failOnError:true);
        }
        break;
      case 1:
        // If we were able to match a unique work, then we know the work unde which any new instances should live
        work = existing_info.possibleWorks.get(0);
        break;
      default:
        throw new RuntimeException("Multiple candidate works - unable to absorb");
        break;
    }

    // PASS 2 - we should have created authoritative instance records by this point
    if ( existing_info.possibleInstances.size() == 0 ) {
      log.debug("Create new instance for ${descriptive_properties.title} - and assign identifiers ${existing_info.unmatchedIdentifiers}");
      Instance new_instance = new Instance(
                                           title: descriptive_properties.title, 
                                           work: work).save(flush:true, failOnError:true);

      existing_info.unmatchedIdentifiers.each { umid ->
        log.debug("Create new instance under work ${work}");
        Authority a = Authority.lookupOrCreate(umid.authority)
        RefdataValue medium = a.impliedMedium != null ? RefdataValue.lookupOrCreate('medium', a.impliedMedium, a.impliedMedium) : null;


        if ( new_instance.medium == null ) {
          // Attempt to infer the medium (Print or electronic) from one of the identifiers authority
          if ( a.impliedMedium != null ) {
            log.debug("Implied medium for new instance is ${a.impliedMedium}");
          }
        }

        log.debug("Register identifier ${umid} against new instance");
        InstanceIdentifier ii = new InstanceIdentifier(instance: new_instance, authority:a, value: InstanceIdentifier.normalise(a, umid.value)).save(flush:true, failOnError:true);
      }
    }
    else if ( existing_info.possibleInstances.size() == 1 ) {
      existing_info.unmatchedIdentifiers.each { umid ->
        Authority a = Authority.lookupOrCreate(umid.authority)
        InstanceIdentifier ii = new InstanceIdentifier(instance: new_instance, authority:a, value: InstanceIdentifier.normalise(a, umid.value)).save(flush:true, failOnError:true);
      }
    }
    else {
      log.debug("Multiple possible instances matches, continue");
    }
  }

  public Map createWorkAndInstances(Map descriptive_record) {
    log.debug("IngestService::createWorkAndInstances(${descriptive_record})");
    return null
  }


  /**
   * Given a list of identifiers, use whatever external registries we have to create authoritative instance records
   * register the instances under the provided work OR create a new work based on the information located.
   */
  public Work fetchAuthoritativeRecords(List identifiers, Work w) {

    Work result = w;

    identifiers.each {
      fetchIdentifier(it)
    }

    return result;
  }

  public void fetchIdentifier(Map identifier) {
    log.debug("Lookup ${identifier}");
    if ( identifier?.authority != null ) {
      Authority a = Authority.lookupOrCreate(identifier.authority)

      // Verify that we really don't have a record for this identifier
      InstanceIdentifier ii = InstanceIdentifier.findByAuthorityAndValue(a, identifier.value)
      if ( ii == null  ) {
        // If not, see if we can look up
        switch ( a.name ) {
          case 'isbn':
            log.debug("Consult ISBN authority (${identifier.value})");
            fetchISBN(identifier,a)
            break;
          case 'issn':
            log.debug("Consult ISSN authority (${identifier.value})");
            fetchISSN(identifier,a)
            break;
        }
      }
      else {
        log.debug("Identifier already present - no action needed");
      }
    }
  }

  // Combine multiple strategies in preference order with
  // Return ( lookupA | lookupB | lookupC )
  public boolean fetchISSN(Map issn, Authority a) {
    return importInstanceFromISSNAgency(issn,a)
  }

  public boolean fetchISBN(Map isbn, Authority a) {
    return importInstanceFromGoogle(isbn,a)
  }

  /**
   * Given an authoritative record from somewhere like the ISSN agency, create a new, or extend an existing
   * instance record
   */
  boolean createOrEnrichInstance(Map instance) {
    Map existing_info = resolverService.bestEffortsMatch(instance);
    Work work = null;
    Instance db_instance = null;

    if ( existing_info.unmatchedIdentifiers?.size() == 0 ) {
      return true; //Nothin to do
    }
   
    if ( ( existing_info.possibleInstances.size() > 1 ) || 
         ( existing_info.possibleWorks.size() > 1 ) ) {
      throw new RuntimeException("Multiple possible instances returned");
    }

    switch ( existing_info.possibleWorks.size() ) {
      case 0:
        // Take no action yet - hopfully one of the external registries will provide us with an authoritative record
        // for the work.
        if ( existing_info.possibleInstances.size() > 0 )
          throw new RuntimeException("Identified work but no instances, should not happen");
        work = new Work(title:instance.title).save(flush:true, failOnError:true);
        break;
      case 1:
        // If we were able to match a unique work, then we know the work unde which any new instances should live
        work = existing_info.possibleWorks.get(0);
        break;
    }

    switch ( existing_info.possibleInstances.size() ) {
      case 0:
        db_instance = new Instance(title: instance.title).save(flush:true, failOnError:true)
        break;
      case 1:
        db_instance = existing_info.possibleInstances.get(0);
        break;
    }
    
    existing_info.unmatchedIdentifiers?.each { unmatched_identifier ->
    }

    return true
  }

  boolean importInstanceFromISSNAgency(Map issn, Authority a) {
    // https://portal.issn.org/resource/ISSN/2639-5983?format=json
    boolean result = false;

    def issn_agency_api = configure {
      request.uri = 'https://portal.issn.org'
    }

    issn_agency_api.get {
      request.uri.path = '/resource/ISSN/'+issn.value
      request.uri.query = [
        'format':'json'
      ]

      response.when(200) { FromServer fs, Object body ->
        log.debug("Book lookup at google responds ${body}")
        result = true;
        // createOrEnrichInstance()
      }

      response.failure { FromServer fs, Object body ->
        log.error("Problem ${body} ${fs} ${fs.getStatusCode()}");
      }
    }

    return true
  }

  /**
   * Be warned: Sometimes the google API matches an ISBN, but does not return that ISBN in the record it found, 
   * so we are forced to infer that the isbn is attached to the item returned... the example search
   * https://www.googleapis.com/books/v1/volumes?q=isbn:9783833484230 demonstrates this.
   */
  boolean importInstanceFromGoogle(Map isbn, Authority a) {
    // https://www.googleapis.com/books/v1/volumes?q=isbn:9783833484230
    boolean result = false;
    def google_books_api = configure {
      request.uri = 'https://www.googleapis.com' // /books/v1/volumes'
    }

    google_books_api.get {
      request.uri.path = '/books/v1/volumes'
      // request.headers.'accept'='application/json'
      request.uri.query = [
        'q':"isbn:${isbn.value}".toString()
      ]

      response.when(200) { FromServer fs, Object body ->
        log.debug("Book lookup at google responds ${body}")
        if ( body.totalItems == 1 ) {
          def instance_record = [
            title: body.items[0].volumeInfo.title,
            identifiers:[ [ 'isbn':isbn.value] ]
          ]

          result = true;
        }
        else {
        }
      }

      response.failure { FromServer fs, Object body ->
        log.error("Problem ${body} ${fs} ${fs.getStatusCode()}");
      }
    }


    return true
  }

}
