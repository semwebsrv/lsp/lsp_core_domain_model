databaseChangeLog = {

  changeSet(author: "ianibbo", id: "extensions-00001") {
    grailsChange {
      change {
        // grailsChange gives us an sql variable which inherits the current connection, and hence should
        // get the schema
        // sql.execute seems to get a bit confused when passed a GString. Work it out before
        sql.execute('CREATE EXTENSION IF NOT EXISTS pg_trgm;');
      }
    }
  }

  include file: 'holocore/changelog.groovy', relativeToChangelogFile:true
  include file: 'lspcore/changelog_1_0_0.groovy', relativeToChangelogFile:true
}
