import groovy.sql.Sql

databaseChangeLog = {

  changeSet(author: "ianibbo (manual)", id: "lsp-core-000-0") {
    grailsChange {
      change {
        // grailsChange gives us an sql variable which inherits the current connection, and hence should
        // get the schema
        // sql.execute seems to get a bit confused when passed a GString. Work it out before
        sql.execute('CREATE EXTENSION IF NOT EXISTS pg_trgm;');
      }
    }
  }

  // Define the migrations for your application here:
  changeSet(author: "ianibbo (manual)", id: "lsp-core-001-1") {

    createTable(tableName: "lsp_work") {
      column(name: "wrk_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "work_pk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "wrk_title",         type: "VARCHAR(2000)")    { constraints(nullable: "false") }
      column(name: "wrk_media_rdv_fk",  type: "VARCHAR(36)")
    }

    createTable(tableName: "lsp_instance") {
      column(name: "ins_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "instance_pk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "ins_title",         type: "VARCHAR(2000)")    { constraints(nullable: "false") }
      column(name: "ins_work_fk",       type: "VARCHAR(36)")
      column(name: 'ins_medium_fk',     type: "VARCHAR(36)")
      column(name: 'ins_edition',       type: "VARCHAR(64)")
      column(name: 'ins_pub_model_fk',  type: "VARCHAR(36)")
    }

    createTable(tableName: "lsp_reading_list") {
      column(name: "rl_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "reading_list_pk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "rl_title",          type: "VARCHAR(2000)")    { constraints(nullable: "false") }
    }

    createTable(tableName: "lsp_reading_list_entry") {
      column(name: "rle_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "reading_list_entry_pk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "rle_title",         type: "VARCHAR(2000)")    { constraints(nullable: "false") }
    }

    grailsChange {
      change {
        // grailsChange gives us an sql variable which inherits the current connection, and hence should
        // get the schema
        // sql.execute seems to get a bit confused when passed a GString. Work it out before
        def work_title_gin_cmd = "CREATE INDEX work_title_gin_idx ON ${database.defaultSchemaName}.lsp_work USING GIN (wrk_title gin_trgm_ops)".toString()
        sql.execute(work_title_gin_cmd);

        def instance_gin_cmd = "CREATE INDEX instance_title_gin_idx ON ${database.defaultSchemaName}.lsp_instance USING GIN (ins_title gin_trgm_ops)".toString()
        sql.execute(instance_gin_cmd);

        confirm 'Gin index on lsp_work(wrk_title) created'
      }
    }
  }

  changeSet(author: "ianibbo (manual)", id: "lsp-core-002-1") {

    createTable(tableName: "lsp_authority") {
      column(name: "auth_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "authority_pk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "auth_name",         type: "VARCHAR(2000)")    { constraints(nullable: "false") }
    }

    createTable(tableName: "lsp_description") {
      column(name: "des_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "description_pk")
      }
      column(name: "version",               type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "des_title",             type: "VARCHAR(2000)")    { constraints(nullable: "false") }
      column(name: "des_owner_authority_fk",type: "VARCHAR(36)")      { constraints(nullable: "false") }
      column(name: "des_data",              type: "bytea")            { constraints(nullable: "false") }
    }

    createTable(tableName: "lsp_instance_identifier") {
      column(name: "ii_id", type: "VARCHAR(36)") {
        constraints(primaryKey: "true", primaryKeyName: "instance_identifier_pk")
      }
      column(name: "version",           type: "BIGINT")           { constraints(nullable: "false") }
      column(name: "ii_instance_fk",    type: "VARCHAR(36)")      { constraints(nullable: "false") }
      column(name: "ii_authority_fk",   type: "VARCHAR(36)")      { constraints(nullable: "false") }
      column(name: "ii_value",          type: "VARCHAR(2000)")    { constraints(nullable: "false") }
    }
  }

  changeSet(author: "ianibbo (manual)", id: "lsp-core-003-1") {
    addColumn(tableName: "lsp_authority") {
      column(name: "auth_lowercase",      type: "BOOLEAN")
      column(name: "auth_nopunc",         type: "BOOLEAN")
      column(name: "auth_trim",           type: "BOOLEAN")
      column(name: "auth_implied_type",   type: "VARCHAR(64)")
      column(name: "auth_implied_media",  type: "VARCHAR(64)")
      column(name: "auth_implied_medium", type: "VARCHAR(64)")
      column(name: "auth_use",            type: "VARCHAR(64)")
    }

    addColumn(tableName: "lsp_instance") {
      column(name: "ins_replaced_by_fk",    type: "VARCHAR(36)")
    }

    addColumn(tableName: "lsp_work") {
      column(name: "wrk_norm_title",    type: "VARCHAR(2000)")
    }
  }

  changeSet(author: "ianibbo (manual)", id: "lsp-core-004-1") {
    addColumn(tableName: "lsp_instance") {
      column(name: "ins_authority",    type: "VARCHAR(36)")
      column(name: "ins_confidence",   type: "VARCHAR(36)")
    }

  }

  changeSet(author: "ianibbo (manual)", id: "lsp-core-005-1") {
    addColumn(tableName: "lsp_reading_list_entry") {
      column(name: "rle_owner_fk",   type: "VARCHAR(36)")
      column(name: "rle_status_fk",  type: "VARCHAR(36)")
      column(name: 'rle_date_first_seen', type: "DATE")
      column(name: 'rle_removed', type: "DATE")
      column(name: 'rle_module_code', type: "VARCHAR(128)")
      column(name: 'rle_order_line_note', type: "VARCHAR(128)")
      column(name: 'rle_number_students', type: "VARCHAR(128)")
      column(name: 'rle_fund_code', type: "VARCHAR(128)")
      column(name: 'rle_location_code', type: "VARCHAR(128)")
      column(name: 'rle_library_code', type: "VARCHAR(128)")
      column(name: 'rle_library_name', type: "VARCHAR(128)")
      column(name: 'rle_exceptions', type: "VARCHAR(128)")
      column(name: 'rle_prb', type: "BOOLEAN")
      column(name: 'rle_priority', type: "VARCHAR(128)")
      column(name: 'rle_suggested_qty', type: "BIGINT")
      column(name: 'rle_num_held', type: "BIGINT")
      column(name: 'rle_isbn', type: "VARCHAR(128)")
      column(name: 'rle_isbn_13', type: "VARCHAR(128)")
      column(name: 'rle_lcn', type: "VARCHAR(128)")
      column(name: 'rle_duplicate_match', type: "VARCHAR(128)")
      column(name: 'rle_note_for_library', type: "VARCHAR(128)")
      column(name: 'rle_note_for_student', type: "VARCHAR(128)")
      column(name: 'rle_edition', type: "VARCHAR(128)")
      column(name: 'rle_pub_date', type: "VARCHAR(128)")
      column(name: 'rle_item_link', type: "VARCHAR(128)")

    }

    addColumn(tableName: "lsp_reading_list") {
      column(name: "rl_code",           type: "VARCHAR(64)")
      column(name: "rl_last_updated",   type: "DATE")
    }
  }

}
