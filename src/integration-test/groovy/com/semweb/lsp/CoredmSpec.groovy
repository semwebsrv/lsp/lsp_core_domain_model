package com.semweb.lsp

import grails.gorm.transactions.*
import grails.testing.mixin.integration.Integration
import groovy.util.logging.Slf4j
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spock.lang.Specification
import spock.lang.Stepwise

import com.semweb.lsp.kb.Instance;
import com.semweb.lsp.kb.Work
import com.semweb.lsp.kb.ResolverService;
import com.semweb.lsp.kb.IngestService;


// @Rollback
@Transactional
@Slf4j
@Stepwise
@Integration
class CoredmSpec extends Specification {

    final static Logger logger = LoggerFactory.getLogger(CoredmSpec.class);

    ResolverService resolverService;
    IngestService ingestService;
    static transactional = true;

    def setup() {
    }

    def cleanup() {
    }

    void "test the database is empty"() {
        expect:"THere are no works"
            Work.list().size() == 0;
    }

    void "test the resolver bestEffortsMatch service"(Map cit, Long instance_count, Long work_count, Long unmatched_identifier_count) {
        when: "we get a citation ${cit} and resolve the works and instances"
          Map matching_result = resolverService.bestEffortsMatch(cit)
          log.debug("Result of resolverService.bestEffortsMatch: ${matching_result}");
        then: "The counts match"
          instance_count == matching_result.possibleInstances.size()
          work_count == matching_result.possibleWorks.size()
          unmatched_identifier_count == matching_result.unmatchedIdentifiers.size()

        then: "If there were unmtched identifiers, try to learn them"
          if ( matching_result.unmatchedIdentifiers.size() > 0 ) {
            log.debug("absorb any new information ${cit} ${matching_result.unmatchedIdentifiers}");
            ingestService.absorb(cit);
          }

        then: "Check that the newly learned identifiers were known ${matching_result.unmatchedIdentifiers}"
          log.debug("Check that the newly learned identifiers were known ${matching_result.unmatchedIdentifiers}")
          matching_result.unmatchedIdentifiers.each { umid ->
            log.debug("Checking ${umid}");
            def i = resolverService.resolveInstanceByIdentifier(umid.authority, umid.value)
            log.debug("Matched instance: ${i}");
            if ( i == null ) {
              log.error("NO INSTANCE MATCH FOR ${cit} / ${matching_result}");
            }
            assert i != null
          }

        then: "See what best efforts returns"
          Map matching_result_2 = resolverService.bestEffortsMatch(cit)
          log.debug("second round matching result:${matching_result_2}");

        where:
          // https://portal.issn.org/resource/ISSN/2639-5983?format=json
          cit|instance_count|work_count|unmatched_identifier_count
          [ title: 'Dance', identifiers: [ [ authority:'issn', value:'2639-5983'] ] ] | 0 | 0 | 1
          [ title: 'Platform For Change', identifiers: [ [ authority:'isbn', value:'0471948403', pubdate:'1994-10-06' ], [ authority:'isbn', value:'9780471948407', pubdate:'1994-10-06' ] ] ] | 0 | 0 | 2
          [ title: 'Platform For Change', identifiers: [ [ authority:'isbn', value:'0471061891', pubdate:'1975-01-01' ], [ authority:'isbn', value:'9780471061892', pubdate:'1975-01-01' ] ] ] | 0 | 0 | 2
    }

}
