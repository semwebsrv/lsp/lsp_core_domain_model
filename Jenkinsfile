#!groovy


podTemplate(
  containers:[
    containerTemplate(name: 'jdk11',                image:'adoptopenjdk:11-jdk-openj9',   ttyEnabled:true, command:'cat'),
    containerTemplate(name: 'postgres12',           image:'postgres:12',                  ttyEnabled:true,
      alwaysPullImage: false,
      // ports:[
      //   portMapping(name: 'postgresql', containerPort: 5432, hostPort: 5432)
      // ],
      envVars: [
        envVar(key: 'POSTGRES_USER', value: 'postgres'),
        envVar(key: 'POSTGRES_PASSWORD', value: 'password'),
        envVar(key: 'POSTGRES_DB', value: 'test')
      ]
    )
  ])
{
  node(POD_LABEL) {

    stage ('checkout') {
      checkout scm
    }
  
    stage ('build LSP domain model') {
      // sh './gradlew --no-daemon --console=plain clean build' - use assemble to build without integration tests
      container('jdk11') {
        try {
          sh 'java --version'
          sh './gradlew --no-daemon --console=plain clean build generatePomFileForMavenPublication'
          sh 'ls ./build/libs'
        }
        catch ( Exception e ) {
          sh 'echo Dumping logs'
          sh 'cat ./build/test-results/integrationTest/*.xml'
          throw e
        }
      }
    }
  
    stage ('publish') {
  
      def props = readProperties file: 'gradle.properties'
      def target_repository = null;
      println("Props: ${props}");
      def release_files = findFiles(glob: '**/lsp_lsp_core_domain_model_master-*.*.*.jar')
      println("Release Files: ${release_files}");
      if ( release_files.size() == 1 ) {
        // println("Release file : ${release_files[0].name}");
        if ( release_files[0].name.contains('SNAPSHOT') ) {
            target_repository='semweb-snapshot';
        }
        else {
            target_repository='semweb-release';
        }
      }
  
      if ( target_repository != null ) {
        println("Publish ${release_files[0].path} with version ${props.appVersion} to ${target_repository}");
        // See https://github.com/jenkinsci/nexus-artifact-uploader-plugin/pull/13 for an example of manually specifying a pom.xml
        // this only works if generatePomFileForMavenPublication is included in the build stage
        nexusArtifactUploader(
          nexusVersion: 'nexus3',
          protocol: 'https',
          nexusUrl: 'nexus.semweb.co',
          groupId: 'com.semweb.lsp',
          version: props.appVersion,
          repository: target_repository,
          credentialsId: 'semweb-nexus',
          artifacts: [
            [artifactId: 'lsp_core_domain_model', classifier: '', file: release_files[0].path, type: 'jar'],
            [artifactId: 'lsp_core_domain_model', type: "pom", file: 'build/publications/maven/pom-default.xml']
          ]
        )
      }
      else {
        println("Target repo is null.. not processing");
      }
    }
  }
}
