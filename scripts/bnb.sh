query1="PREFIX bibo: <http://purl.org/ontology/bibo/> \
SELECT ?uri WHERE { \
  ?uri bibo:isbn10 \"0261102214\". \
}"

query2=" PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
CONSTRUCT { \
    <http://bnb.data.bl.uk/id/resource/005711109> ?var_1_0_prop ?var_1_0 . \
    ?var_1_0 rdfs:label ?var_1_1_0 . \
}  WHERE {  \
  { \
    <http://bnb.data.bl.uk/id/resource/005711109> ?var_1_0_prop ?var_1_0 . \
    OPTIONAL { ?var_1_0 rdfs:label ?var_1_1_0 . } \
  }  \
} "

curl \
    --data-urlencode "query=$query2" \
    --data-urlencode "output=json" \
    "https://bnb.data.bl.uk/sparql"


echo "https://bnb.data.bl.uk/doc/resource/005711109"

