# lsp_core_domain_model


## Developer loop for domain model

This domain model comes with a set of integration tests that validate the migrations and the core domain classes
Please ensure these pass before merging commits onto the master or other public branch

To run the developer integration tests
  
  source ./env.sh           // Provision right grails, java, gradle env
  docker-compose down -v    // To ensure you have a clean environment
  grails test-app           // build and test

This is the domain model for library services



testcontainers docs: https://kiview.github.io/testcontainers-grails-workshop/


Data Source APIs
https://portal.issn.org/resource/ISSN/2639-5983?format=json
https://www.googleapis.com/books/v1/volumes?q=isbn:9780471948407

https://pinboard.in/u:ianibbo/t:datasource/

